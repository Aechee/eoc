from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .models import RegisterUserModel

from .forms import RegisterUserForm

from django.contrib.auth.decorators import login_required

@login_required
def register_user(request):
    if request.method == "POST":
        _feed_back = RegisterUserForm(request.POST)
        if _feed_back.is_valid():
            obj = _feed_back.save(commit=False)
            obj.from_user = request.user
            obj.save()
            form = RegisterUserForm()

            #form = RegisterUser()
            #return HttpResponseRedirect('registeruser_form.html')

            return HttpResponseRedirect(reverse("register_user"))

            #return render(request, "registerapp/registeruser_form.html", {'form': form})
    else:
        form = RegisterUserForm()
    return render(request, "registerapp/registeruser_form.html", {'form': form})

@login_required
def get_users(request):
    all_users = RegisterUserModel.objects.all_reg_users()
    return render(request, "registerapp/show_users.html",{'allusers':all_users})
