from django.conf.urls import url

from .views import register_user, get_users

urlpatterns = [
    url(r'^register_user$', register_user, name='register_user'),
    url(r'showUsers$',get_users, name="reg_users"),
]