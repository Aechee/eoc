from django import forms
from .models import RegisterUserModel
from django.forms import ModelForm, DateInput

class RegisterUserForm(ModelForm):
    class Meta:
        model = RegisterUserModel
        widgets = {
            'date_upec': DateInput(attrs={'type': 'date'}),
            'date_training': DateInput(attrs={'type': 'date'}),
            'map_receiving_date': DateInput(attrs={'type': 'date'}),
            'returned_maps_date': DateInput(attrs={'type': 'date'})

        }
        exclude = ['time','from_user',]
