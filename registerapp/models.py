from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
import datetime
import psycopg2
from feedback import settings

_CHOICE_UC = ((1,'Allahabad'),(2,'Thariri Muhabat'),(3,'Shah Panjo'),(4,'Radhan'),(5,'Qazi Arif'),(6,'Nau Goth'),(7,'Mehar'))
_CHOICE_MONTH = ((1,'January'),(2,'Febuary'),(3,'March'),(4,'April'),(5,'May'),(6,'June'),(7,'July'))
_CHOICE_GENDER = (('F','Female'),('M','Male'))
_CHOICE_TPL = (('Y','Yes'),('N','No'))
_CHOICE_YES_NO = ((1,'Yes'),(0,'No'))
RATING_CHOICES = ((0, 'Never used'),(1, 'Bad'),(2, 'Below Average'),(3, 'Average'),(4, 'Above Average'),(5, 'Good'))

class RegisterUserQuerySet(models.QuerySet):
    def all_reg_users(self):
        return self.all()

class RegisterUserModel(models.Model):
    def getLookupValue(self,type):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        try:
            conn = psycopg2.connect(database=dbname,
                                    user=username,host=host,
                                    password=password,port=port)
            #conn.set_isolation_level(ISOLEVEL)
            cursor = conn.cursor()
            if(type=='dist'):
                cursor.execute("select district,district from lookup group by district order by district")
            elif(type=='tehs'):
                cursor.execute("select tehsil,tehsil from lookup group by tehsil order by tehsil")
            elif(type=='uc'):
                cursor.execute("select uc,uc from lookup group by uc order by uc")
            fetch = cursor.fetchall()

            #dblist = [fetch[i][0] for i in range(len(fetch))]
            lookuplist = []
            for i in range(len(fetch)):
                lookuplist.append((fetch[i][1], fetch[i][0]))
            print('hello world')
        except psycopg2.Error as e:
            print(e)
        finally:
            conn.close()
        #dist_list = (('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
        return lookuplist
    uc_name = models.IntegerField(default= '0',choices=getLookupValue('lookup','uc'),null=False, verbose_name="Union Council Name")
    ucmo_name = models.CharField(max_length=30,null=False,verbose_name="Name of UCMO")
    current_aics = models.IntegerField(default='0',verbose_name="Total Number of Current AICs")
    aics_in_maps = models.IntegerField(default='0',verbose_name="Total Number of AICs in the Map")
    total_no_mob_teams = models.IntegerField(default='0',verbose_name="Total Number of Mobile Teams")
    date_upec = models.DateField(blank=True,default=datetime.datetime.now, verbose_name="Date of UPEC")
    date_training = models.DateField(blank=True,default=datetime.datetime.now, verbose_name="Date of Training")
    map_receiving_date = models.DateField(blank=True,default=datetime.datetime.now, verbose_name="GIS Map Receiving Date")
    name_person = models.CharField(max_length=30, null=True, verbose_name="Name of person who provided map")
    user_of_maps = models.IntegerField(default="0", choices=_CHOICE_YES_NO,
                                    verbose_name="Have you briefed on the use of GIS Map to AICs during UPEC?")
    name_of_trainer = models.CharField(max_length=30, null=True)
    maps_use_planning = models.IntegerField(default="0", choices=_CHOICE_YES_NO,
                                         verbose_name="Have GIS maps been used in Microplanning?")
    last_used = models.IntegerField(default='0', choices=_CHOICE_MONTH, null=False,
                                    verbose_name="When was this last used? Campaign/month:")

    total_settlements = models.IntegerField(default='0',verbose_name="Settlements have found on GIS Map but missing in micro-plan. Given total number in figure")
    name_settlements = models.IntegerField(default='0',verbose_name="Write names of such settlements / write name on the map")

    text_field = models.CharField(max_length=30, null=True, verbose_name="Have you made AICs level readjustments / changes and marked on GIS Maps in order to improve Micro-planning?")
    admin_changes = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                    verbose_name="Administrative changes (UC boundaries)")
    geo_changes = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                        verbose_name="Geographic changes (settlement naming)")
    area_adjustment = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                      verbose_name="Area distribution adjustments")
    jurisdictional_changes = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                      verbose_name="Jurisdictional changes")
    unification_scattered_areas = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                      verbose_name="Unification of scattered areas")
    coverage_missed_areas = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                                      verbose_name="Coverage of missed areas")
    returned_maps = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                                verbose_name="Have you returned marked GIS Maps for fresh edition?")
    returned_maps_person = models.CharField(max_length=30, null=True, verbose_name="To Whom marked GIS Maps have returned: Name")
    returned_maps_designation = models.CharField(max_length=30, null=True,
                                            verbose_name="Designation")
    returned_maps_date = models.DateField(blank=True,default=datetime.datetime.now,verbose_name="Maps Returned Date")
    mode_of_returned = models.CharField(max_length=30, null=True,
                                                 verbose_name="Mode of return")
    missing_detail_map = models.CharField(max_length=30, null=True,
                                  verbose_name="Have you found any missing detail on GIS Map? If so then give detail:")
    settlements = models.CharField(max_length=30, null=True,verbose_name="Settlement/s")
    settlements_total = models.IntegerField(default='0',verbose_name="Settlements Total")

    water_features = models.CharField(max_length=30, null=True, verbose_name="Water Feature e.g., canal/river/Dam & Roads")
    water_features_total = models.IntegerField(default='0', verbose_name="Total")

    name_labels_legend = models.CharField(max_length=30, null=True, verbose_name="Names/ Labels/ Legend:")
    name_labels_legend_total = models.IntegerField(default='0', verbose_name="Total")
    other_required_on_map = models.CharField(max_length=30, null=True,
                                  verbose_name="Other if required on map")
    high_risk_mob_pop = models.IntegerField(default='0', choices=_CHOICE_YES_NO, null=False,
                                                verbose_name="Have you located “High Risk Mobile Population” on the map?")
    high_risk_mob_pop_total = models.IntegerField(default='0', verbose_name="Total")
    high_risk_mob_pop_details = models.CharField(max_length=30, null=True,
                                  verbose_name="If so then give detail:")
    issues_with_technology = models.CharField(max_length=30, null=True,
                                                 verbose_name="Descriptive detail on issues and the use of  new technology: ")
    suggestion_proposals = models.CharField(max_length=30, null=True,
                                              verbose_name="Suggestions or proposal")

    from_user = models.CharField(max_length=30, null=True)


    # know_tpl = models.BooleanField(verbose_name="Tick if you know about TPL!")
    know_tpl = models.CharField(max_length=1, default="N", choices=_CHOICE_TPL, verbose_name="Do you Know about TPL?")
    rating = models.IntegerField(default= '0',choices=RATING_CHOICES,validators=[MinValueValidator(0),MaxValueValidator(5)])
    comment = models.CharField(max_length=300,
                               blank=True,
                               verbose_name="Comment (Optional)",
                               help_text="It would help us evolve!")
    #time = models.DateTimeField(auto_now_add=True)
    objects = RegisterUserQuerySet.as_manager()


