from django.forms import ModelForm, DateInput
from .models import MapReceive

class MapReceiveForm(ModelForm):
    class Meta:
        model = MapReceive
        widgets = {
            'map_date': DateInput(attrs={'type': 'date'}),
            'handed_over_date': DateInput(attrs={'type': 'date'}),
            'taken_over_date': DateInput(attrs={'type': 'date'})
        }

        exclude = ['time','from_user',]