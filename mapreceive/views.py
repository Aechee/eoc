import json
from collections import Counter

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse

from mapreceive.models import MapReceive
from .forms import MapReceiveForm
from django.contrib.auth.forms import UserCreationForm

from django.views.generic import CreateView
from django.contrib.auth.decorators import login_required

@login_required
def new_mapreceive(request):
    if request.method == "POST":
        _mapreceive = MapReceiveForm(request.POST)
        if _mapreceive.is_valid():
            #_feed_back.cleaned_data['from_user'] = str(request.user)
            #_feed_back.fields["from_user"].initial = str(request.user)
            obj = _mapreceive.save(commit=False)
            obj.from_user = request.user
            obj.save()
            form = MapReceiveForm()
            #return render(request, "feedback/feedback_form.html", {'form': form})

            return HttpResponseRedirect(reverse("new_mapreceive"))
    else:
        form = MapReceiveForm()
    return render(request, "mapreceive/mapreceive_form.html", {'form': form})

@login_required
def home(request):
    all_mapreceive = MapReceive.objects.all_mapreceive()
    return render(request, "mapreceive/showMapReceive.html",{'mapreceive':all_mapreceive})