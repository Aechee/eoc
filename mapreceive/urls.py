from django.conf.urls import url
from django.contrib.auth.views import LoginView,LogoutView

from .views import new_mapreceive, home

urlpatterns = [
    url(r'^mapreceive$', new_mapreceive, name='new_mapreceive'),
    url(r'login$', LoginView.as_view(template_name='feedback/login_form.html'), name='player_login'),
    url(r'^$', LoginView.as_view(template_name='feedback/login_form.html'), name='player_login'),
    url(r'logout$',LogoutView.as_view(),name='feedback_logout'),
   # url(r'signup$', SignupView.as_view(),name='player_signup'),
    url(r'showMapReceive',home, name="player_home"),
]
