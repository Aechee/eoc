from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from feedback import settings
from django.contrib.postgres import fields
from django.db.models import Q
import datetime
import psycopg2

_CHOICE_GENDER = (('F','Female'),('M','Male'))
_CHOICE_TPL = (('Y','Yes'),('N','No'))
RATING_CHOICES = ((0, 'Never used'),(1, 'Bad'),(2, 'Below Average'),(3, 'Average'),(4, 'Above Average'),(5, 'Good'))
_DIST_LIST = (('Mirpur', 'Mirpur'),('Khairpur', 'Khairpur'),('Shikarpur', 'Shikarpur'),('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
_ACTIVITY_LIST = (('Activity_1','Activity_1'),('Activity_2','Activity_2'),('Activity_3','Activity_3'),('Activity_4','Activity_4'))
_TALUKA_LIST = (('Taluka-1', 'Taluka-1'),('Taluka-2', 'Taluka-2'),('Taluka-3', 'Taluka-3'),('Taluka-4', 'Taluka-4'),('Taluka-5', 'Taluka-5'))
_UC_LIST = (('UC-1', 'UC-1'),('UC-2', 'UC-2'),('UC-3', 'UC-3'),('UC-4', 'UC-4'),('UC-5', 'UC-5'))


class MapReceiveQuerySet(models.QuerySet):
    def all_mapreceive(self):
        return self.all()

class MapReceive(models.Model):
    def getLookupValue(self,type):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        try:
            conn = psycopg2.connect(database=dbname,
                                    user=username,host=host,
                                    password=password,port=port)
            #conn.set_isolation_level(ISOLEVEL)
            cursor = conn.cursor()
            if(type=='dist'):
                cursor.execute("select district,district from lookup group by district order by district")
            elif(type=='tehs'):
                cursor.execute("select tehsil,tehsil from lookup group by tehsil order by tehsil")
            elif(type=='uc'):
                cursor.execute("select uc,uc from lookup group by uc order by uc")
            fetch = cursor.fetchall()

            #dblist = [fetch[i][0] for i in range(len(fetch))]
            lookuplist = []
            for i in range(len(fetch)):
                lookuplist.append((fetch[i][1], fetch[i][0]))
            print('hello world')
        except psycopg2.Error as e:
            print(e)
        finally:
            conn.close()
        #dist_list = (('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
        return lookuplist

    from_user = models.CharField(max_length=30, null=False)

    sop_no = models.CharField(max_length=30,default="00-00",verbose_name="SOP No.")
    map_date = models.DateField(blank=True,default=datetime.datetime.now, verbose_name="Map Receiving Date")
    #activity = models.CharField(max_length=30,default="Activity_1",choices=_ACTIVITY_LIST,verbose_name="Choose Activity")
    dist = models.CharField(max_length=30,default="Karachi", choices=getLookupValue('lookup','dist'), verbose_name="District")
    taluka = models.CharField(max_length=30,default="Taluka-1", choices=getLookupValue('lookup','tehs'), verbose_name="Taluka")
    uc = models.CharField(max_length=30,default="UC-1", choices=getLookupValue('lookup','uc'), verbose_name="Union Council")

    from_office = models.CharField(max_length=30, default="", verbose_name="From")
    to_office = models.CharField(max_length=30 , default="", verbose_name="To")

    map_name_1 = models.CharField(max_length=30, default="",verbose_name="1. Map Name")
    serial_from_1 = models.IntegerField(default='0',verbose_name="1. Serial No. (From)")
    serial_to_1 = models.IntegerField(default='0', verbose_name="1. Serial No. (To)")
    copies_1 = models.IntegerField(default='0',verbose_name="1. Copies")
    quantity_1 = models.IntegerField(default='0',verbose_name="1. Quantity")
    remarks_1 = models.CharField(max_length=30, default="",verbose_name="1. Remarks")

    map_name_2 = models.CharField(max_length=30, default="", verbose_name="2. Map Name")
    serial_from_2 = models.IntegerField(default='0', verbose_name="2. Serial No. (From)")
    serial_to_2 = models.IntegerField(default='0', verbose_name="2. Serial No. (To)")
    copies_2 = models.IntegerField(default='0', verbose_name="2. Copies")
    quantity_2 = models.IntegerField(default='0', verbose_name="2. Quantity")
    remarks_2 = models.CharField(max_length=30, default="", verbose_name="2. Remarks")

    map_name_3 = models.CharField(max_length=30, default="", verbose_name="3. Map Name")
    serial_from_3 = models.IntegerField(default='0', verbose_name="3. Serial No. (From)")
    serial_to_3 = models.IntegerField(default='0', verbose_name="3. Serial No. (To)")
    copies_3 = models.IntegerField(default='0', verbose_name="3. Copies")
    quantity_3 = models.IntegerField(default='0', verbose_name="3. Quantity")
    remarks_3 = models.CharField(max_length=30, default="", verbose_name="3. Remarks")

    map_name_4 = models.CharField(max_length=30, default="", verbose_name="4. Map Name")
    serial_from_4 = models.IntegerField(default='0', verbose_name="4. Serial No. (From)")
    serial_to_4 = models.IntegerField(default='0', verbose_name="4. Serial No. (To)")
    copies_4 = models.IntegerField(default='0', verbose_name="4. Copies")
    quantity_4 = models.IntegerField(default='0', verbose_name="4. Quantity")
    remarks_4 = models.CharField(max_length=30, default="", verbose_name="4. Remarks")

    map_name_5 = models.CharField(max_length=30, default="", verbose_name="5. Map Name")
    serial_from_5 = models.IntegerField(default='0', verbose_name="5. Serial No. (From)")
    serial_to_5 = models.IntegerField(default='0', verbose_name="5. Serial No. (To)")
    copies_5 = models.IntegerField(default='0', verbose_name="5. Copies")
    quantity_5 = models.IntegerField(default='0', verbose_name="5. Quantity")
    remarks_5 = models.CharField(max_length=30, default="", verbose_name="5. Remarks")

    map_name_6 = models.CharField(max_length=30, default="", verbose_name="6. Map Name")
    serial_from_6 = models.IntegerField(default='0', verbose_name="6. Serial No. (From)")
    serial_to_6 = models.IntegerField(default='0', verbose_name="6. Serial No. (To)")
    copies_6 = models.IntegerField(default='0', verbose_name="6. Copies")
    quantity_6 = models.IntegerField(default='0', verbose_name="6. Quantity")
    remarks_6 = models.CharField(max_length=30, default="", verbose_name="6. Remarks")

    handed_over_name = models.CharField(max_length=30, default="", null=False, verbose_name="Handed Over Name")
    handed_over_desig = models.CharField(max_length=30, default="",null=False,verbose_name="Handed Over Designation")
    handed_over_date = models.DateField(blank=True,default=datetime.datetime.now,verbose_name="Handed Over Date")

    taken_over_name = models.CharField(max_length=30, default="", null=False, verbose_name="Taken Over Name")
    taken_over_desig = models.CharField(max_length=30, default="", null=False, verbose_name="Taken Over Designation")
    taken_over_date = models.DateField(blank=True,default=datetime.datetime.now, verbose_name="Taken Over Date")
    objects = MapReceiveQuerySet.as_manager()