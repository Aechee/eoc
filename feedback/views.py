from django.shortcuts import render, redirect

def welcome(request):
    if request.user.is_authenticated:
        return redirect('new_feedback')
    else:
        return render(request, 'tictac/welcome.html')