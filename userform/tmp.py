from_user = models.CharField(max_length=30, null=False)

dist = models.IntegerField(default='0', choices=_DIST_LIST, verbose_name="District")
taluka = models.IntegerField(default='0', choices=_TALUKA_LIST, verbose_name="Taluka")
uc = models.IntegerField(default='0', choices=_UC_LIST, verbose_name="Union Council")
name = models.CharField(max_length=30, null=False, verbose_name="Name")
contact = models.IntegerField(default='0', verbose_name="Contact No.")

time = models.DateTimeField(auto_now_add=True)
visit_date = models.DateField(blank=True, verbose_name="Last time used")
objects = FeedbackQuerySet.as_manager()