from django.forms import ModelForm, DateInput
from .models import Feedback, FeedbackMeta, Surveyors, TeamLeads
from django import forms
from feedback import settings
import psycopg2
class FeedbackForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)
        self.fields["activity"].choices= self.get_Activities()
        self.fields["Surveyor"].choices = self.getSurveyorsLookup()
        self.fields["TeamLeads"].choices = self.getTeamLeadsLookup()

        for key in self.fields:
            self.fields[key].required = True
    def getTeamLeadsLookup(self):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)
        #conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute('select "TL_Name","TL_Name" from userform_teamleads order by "TL_Name"')
        fetch = cursor.fetchall()

        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append((fetch[i][1], fetch[i][0]))
        conn.close()
        return lookuplist


    def getSurveyorsLookup(self):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)
        #conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute('select "Sur_Name","Sur_Name" from userform_surveyors order by "Sur_Name"')
        fetch = cursor.fetchall()

        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append((fetch[i][1], fetch[i][0]))
        conn.close()
        return lookuplist

    def get_Activities(self):
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']

        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)

        cursor = conn.cursor()
        cursor.execute("select activity,activity from userform_feedbackmeta order by activity")

        fetch= cursor.fetchall()
        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append((fetch[i][1], fetch[i][0]))
        return lookuplist

    activity= forms.ChoiceField(choices=get_Activities('lookup'),)
    Surveyor = forms.ChoiceField(choices=getSurveyorsLookup('lookup'), )
    TeamLeads = forms.ChoiceField(choices=getTeamLeadsLookup('lookup'), )


    class Meta:
        model = Feedback

        widgets = {
            'visit_date': DateInput(attrs={'type': 'date'})
        }

        exclude = ['time','from_user',]


class AddUcsMeta(forms.Form):
    division_name = forms.CharField(max_length=60)
    District_Name = forms.CharField(max_length=60)
    Taluka_Name= forms.CharField(max_length=60)
    Union_Councile_Name= forms.CharField(max_length=60)
    Low_Priority = forms.BooleanField()




class FeedbackFormMeta(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FeedbackFormMeta, self).__init__(*args, **kwargs)
        self.fields["Surveyors"].choices = self.getSurveyorsLookup()
        self.fields["TeamLeads"].choices = self.getTeamLeadsLookup()
        for key in self.fields:
            self.fields[key].required = True

    def getTeamLeadsLookup(self):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)
        #conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute('select "TL_Name","TL_Name" from userform_teamleads order by "TL_Name"')
        fetch = cursor.fetchall()

        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append((fetch[i][1], fetch[i][0]))
        conn.close()
        return lookuplist


    def getSurveyorsLookup(self):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)
        #conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute('select "Sur_Name","Sur_Name" from userform_surveyors order by "Sur_Name"')
        fetch = cursor.fetchall()

        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append((fetch[i][1], fetch[i][0]))
        conn.close()
        return lookuplist

    def getLookupValue(self,type):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        try:
            conn = psycopg2.connect(database=dbname,
                                    user=username,host=host,
                                    password=password,port=port)
            #conn.set_isolation_level(ISOLEVEL)
            cursor = conn.cursor()
            if(type=='dist'):
                cursor.execute("select district,district from lookup group by district order by district")
            elif(type=='tehs'):
                cursor.execute("select tehsil,tehsil from lookup group by tehsil order by tehsil")
            elif(type=='uc'):
                cursor.execute("select uc,uc from lookup group by uc order by uc")
            fetch = cursor.fetchall()

            #dblist = [fetch[i][0] for i in range(len(fetch))]
            lookuplist = []
            for i in range(len(fetch)):
                lookuplist.append((fetch[i][1], fetch[i][0]))
            print('hello world')
        except psycopg2.Error as e:
            print(e)
        finally:
            conn.close()
        #dist_list = (('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
        return lookuplist

    OPTIONS = getLookupValue('lookup','uc')
    uc = forms.MultipleChoiceField(widget=forms.SelectMultiple,
                                   choices=OPTIONS)
    Surveyors = forms.MultipleChoiceField(widget=forms.SelectMultiple,
                                   choices=getSurveyorsLookup('lookup'))
    TeamLeads = forms.MultipleChoiceField(widget=forms.SelectMultiple,
                                   choices=getTeamLeadsLookup('lookup'))

    class Meta:
        model = FeedbackMeta
        widgets = {
            'visit_date': DateInput(attrs={'type': 'date'})
        }

        exclude= []



class SurveyorMetaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SurveyorMetaForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].required = True
    class Meta:
        model = Surveyors
        widgets = {
            'Sur_Date': DateInput(attrs={'type': 'date'})
        }

        exclude= []

class TeamLeadsMetaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(TeamLeadsMetaForm, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = True
    class Meta:
        model = TeamLeads
        widgets = {
            'TL_Date': DateInput(attrs={'type': 'date'})
        }

        exclude= []
