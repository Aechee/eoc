import json
from collections import Counter

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse

from userform.models import Feedback
from .forms import *
from django.contrib.auth.forms import UserCreationForm

from django.views.generic import CreateView
from django.contrib.auth.decorators import login_required
from feedback import settings
import psycopg2
import ast


# index page to redirect after login
@login_required
def index(request):
    return render(request, "feedback/index.html")


@login_required
def new_feedback(request):
    if request.method == "POST":
        _feed_back = FeedbackForm(request.POST)
        if _feed_back.is_valid():

            obj = _feed_back.save(commit=False)
            obj.from_user = request.user
            obj.save()
            form = FeedbackForm()
            #return render(request, "feedback/feedback_form.html", {'form': form})

            return HttpResponseRedirect(reverse("new_feedback"))
    else:
        form = FeedbackForm()
    return render(request, "feedback/feedback_form.html", {'form': form})

@login_required
def surveyor_handle(request):
    if request.method == "POST":
        _feed_back = SurveyorMetaForm(request.POST)
        if _feed_back.is_valid():

            obj = _feed_back.save(commit=False)
            obj.from_user = request.user
            obj.save()
            form = SurveyorMetaForm()
            #return render(request, "feedback/feedback_form.html", {'form': form})

            return HttpResponseRedirect(reverse("surveyor_handle"))
    else:
        form = SurveyorMetaForm()
    return render(request, "feedback/addSurveyors.html", {'form': form})

@login_required
def teamleads_handle(request):
    if request.method == "POST":
        _feed_back = TeamLeadsMetaForm(request.POST)
        if _feed_back.is_valid():

            obj = _feed_back.save(commit=False)
            obj.from_user = request.user
            obj.save()
            form = TeamLeadsMetaForm()
            #return render(request, "feedback/feedback_form.html", {'form': form})

            return HttpResponseRedirect(reverse("teamLeads_handle"))
    else:
        form = TeamLeadsMetaForm()
    return render(request, "feedback/addTeamLeads.html", {'form': form})


@login_required
def meta_feedback(request):
    if request.method == "POST":
        _feed_back = FeedbackFormMeta(request.POST)
        if _feed_back.is_valid():
            obj = _feed_back.save(commit=False)
            obj.from_user = request.user
            obj.save()
            form = FeedbackFormMeta()

            return HttpResponseRedirect(reverse("meta_feedback"))
    else:
        form = FeedbackFormMeta()
    return render(request, "feedback/feedback_meta.html", {'form': form})



@login_required
def showFirstReport(request):
    def getFirstReport():
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        # try:
        conn = psycopg2.connect(database=dbname,
                                user=username, host=host,
                                password=password, port=port)
        # conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute(" select * from vw_uc_cnt ")
        fetch = cursor.fetchall()
        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append([i+1,fetch[i][0],fetch[i][1]])

        return lookuplist
    all_feedback=getFirstReport()
    return render(request, "feedback/firstreport.html",{'feedback':all_feedback})

@login_required
def showSecondReport(request):
    def getFirstReport():
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        # try:
        conn = psycopg2.connect(database=dbname,
                                user=username, host=host,
                                password=password, port=port)
        # conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute(" select * from vw_uc_status ")
        fetch = cursor.fetchall()
        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append([i+1,fetch[i][0],fetch[i][1]])

        return lookuplist
    all_feedback=getFirstReport()
    return render(request, "feedback/secondreport.html",{'feedback2':all_feedback})

@login_required
def showUCs(request):
    def getFirstReport():
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        # try:
        conn = psycopg2.connect(database=dbname,
                                user=username, host=host,
                                password=password, port=port)
        # conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute(" select division, district, tehsil, uc from LOOKUP ")
        fetch = cursor.fetchall()
        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append([i+1,fetch[i][0],fetch[i][1],fetch[i][2],fetch[i][3],fetch[i][3]])

        return lookuplist
    all_feedback=getFirstReport()
    return render(request, "feedback/showUC.html",{'feedback2':all_feedback})



@login_required
def showSurveyors(request):
    feedback= Surveyors.objects.all()
    return render(request, "feedback/showSurveyors.html",{'feedback':feedback})

@login_required
def showTeamLeads(request):
    feedback= TeamLeads.objects.all()
    return render(request, "feedback/showTeamLeads.html",{'feedback':feedback})


@login_required
def home(request):
    all_feedback = Feedback.objects.all_feedback()
    return render(request, "feedback/showFeedback.html",{'feedback':all_feedback})

def update_talukaFn(request):
    district = request.GET.get('district', None)
    new_values=getFilterValues('dist',district)

    # sub_category = ['test','test','test','test']
    return JsonResponse(new_values, safe=False)

def update_ucFn(request):
    tehsil = request.GET.get('tehsil', None)
    new_values=getFilterValues('tehs',tehsil)
    return JsonResponse(new_values, safe=False)

def update_activitites(request):
    new_values=get_Activities()
    return JsonResponse(new_values, safe=False)

def update_districts(request):
    activity = request.GET.get('activity', None)
    new_values=getFeedBackDetails('activity',activity)
    # test_ls=[[["data"],["data2"]],["test1"],["test2"]]
    return JsonResponse(new_values, safe=False)

def update_taluka_feedback(request):
    activity = request.GET.get('activity', None)
    district = request.GET.get('district', None)
    new_values=getActivityTalukas(activity,district)
    return JsonResponse(new_values, safe=False)

def update_uc_feedback(request):
    activity = request.GET.get('activity', None)
    district = request.GET.get('district', None)
    taluka = request.GET.get('taluka', None)
    new_values=getUCActivities(activity,district,taluka)
    return JsonResponse(new_values, safe=False)

def update_surveyors_feedback(request):
    activity = request.GET.get('activity', None)
    district = request.GET.get('district', None)
    taluka = request.GET.get('taluka', None)
    new_values=getActivitySurveyors(activity,district,taluka)
    return JsonResponse(new_values, safe=False)

def update_teamleads_feedback(request):
    activity = request.GET.get('activity', None)
    district = request.GET.get('district', None)
    taluka = request.GET.get('taluka', None)
    new_values=getActivityTeamLeads(activity,district,taluka)
    return JsonResponse(new_values, safe=False)



def getFeedBackDetails(type,value):
    dbname = settings.DATABASES['default']['NAME']
    username = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']
    # try:
    conn = psycopg2.connect(database=dbname,
                            user=username,host=host,
                            password=password,port=port)
    #conn.set_isolation_level(ISOLEVEL)
    cursor = conn.cursor()
    if(type=='activity'):
        cursor.execute(
            "select distinct dist from userform_feedbackmeta where upper(activity)= '" + value.upper() + "' ")
        #cursor.execute("select dist,taluka,uc from userform_feedbackmeta where upper(activity)= '"+value.upper()+"' ")
    fetch=cursor.fetchall()
    # data=[]
    #
    # for value in fetch:
    #     data.append([value[0]])
    #     data.append([value[1]])
    #
    #     data.append(ast.literal_eval(value[2]))
    #
    # return data;
    lookuplist = []

    for i in range(len(fetch)):
        lookuplist.append(fetch[i][0])

    return lookuplist

def getActivityTalukas(activity, district):

    dbname = settings.DATABASES['default']['NAME']
    username = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']
    # try:
    conn = psycopg2.connect(database=dbname,
                            user=username,host=host,
                            password=password,port=port)
    #conn.set_isolation_level(ISOLEVEL)
    cursor = conn.cursor()
    cursor.execute("select distinct taluka from userform_feedbackmeta where upper(activity)='"+activity.upper()+"' and upper(dist)='"+district.upper()+"' order by taluka")
    fetch=cursor.fetchall()
    lookuplist = []
    for data in fetch:
        lookuplist.append(data[0])

    #lookuplist=ast.literal_eval(fetch[0][0])
    return lookuplist



def getUCActivities(activity, district,taluka):

    dbname = settings.DATABASES['default']['NAME']
    username = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']
    # try:
    conn = psycopg2.connect(database=dbname,
                            user=username,host=host,
                            password=password,port=port)
    #conn.set_isolation_level(ISOLEVEL)
    cursor = conn.cursor()

    cursor.execute("select uc from userform_feedbackmeta where upper(activity)= '" + activity.upper() + "' and upper(dist)='"+district.upper()+"' and upper(taluka)= '"+taluka.upper()+"'")

    fetch=cursor.fetchall()
    lookuplist = []

    for i in range(len(fetch)):
        data_ls= ast.literal_eval(fetch[i][0])
        for uc in data_ls:
            lookuplist.append(uc)
        #lookuplist.append(fetch[i][0])

    return lookuplist

def getActivitySurveyors(activity, district,taluka):

    dbname = settings.DATABASES['default']['NAME']
    username = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']
    # try:
    conn = psycopg2.connect(database=dbname,
                            user=username,host=host,
                            password=password,port=port)
    #conn.set_isolation_level(ISOLEVEL)
    cursor = conn.cursor()

    cursor.execute("select \"Surveyors\" from userform_feedbackmeta where upper(activity)= '" + activity.upper() + "' and upper(dist)='"+district.upper()+"' and upper(taluka)= '"+taluka.upper()+"'")

    fetch=cursor.fetchall()
    lookuplist = []

    for i in range(len(fetch)):
        data_ls= ast.literal_eval(fetch[i][0])
        for uc in data_ls:
            lookuplist.append(uc)
        #lookuplist.append(fetch[i][0])

    return lookuplist

def getActivityTeamLeads(activity, district,taluka):

    dbname = settings.DATABASES['default']['NAME']
    username = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']
    # try:
    conn = psycopg2.connect(database=dbname,
                            user=username,host=host,
                            password=password,port=port)
    #conn.set_isolation_level(ISOLEVEL)
    cursor = conn.cursor()

    cursor.execute("select \"TeamLeads\" from userform_feedbackmeta where upper(activity)= '" + activity.upper() + "' and upper(dist)='"+district.upper()+"' and upper(taluka)= '"+taluka.upper()+"'")

    fetch=cursor.fetchall()
    lookuplist = []

    for i in range(len(fetch)):
        data_ls= ast.literal_eval(fetch[i][0])
        for uc in data_ls:
            lookuplist.append(uc)
        #lookuplist.append(fetch[i][0])

    return lookuplist


# Function that will return the activities stored in the database
def get_Activities():
    dbname = settings.DATABASES['default']['NAME']
    username = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']

    conn = psycopg2.connect(database=dbname,
                            user=username,host=host,
                            password=password,port=port)
    #conn.set_isolation_level(ISOLEVEL)
    cursor = conn.cursor()
    cursor.execute("select distinct activity from userform_feedbackmeta order by activity")
    fetch= cursor.fetchall()
    lookuplist = []
    for i in range(len(fetch)):
        lookuplist.append(fetch[i][0])
    return lookuplist



# function to return the values for ajax calls
def getFilterValues(type,value):
    #print(settings.DATABASES)
    dbname = settings.DATABASES['default']['NAME']
    username = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']
    # try:
    conn = psycopg2.connect(database=dbname,
                            user=username,host=host,
                            password=password,port=port)

    cursor = conn.cursor()
    if(type=='dist'):
        cursor.execute("select distinct tehsil from lookup where upper(district)= '"+value.upper()+"' ")
    elif(type=='tehs'):
        cursor.execute("select distinct uc from lookup where upper(tehsil)='"+value.upper()+"' ")
    fetch = cursor.fetchall()

    #dblist = [fetch[i][0] for i in range(len(fetch))]
    lookuplist = []
    for i in range(len(fetch)):
        lookuplist.append((fetch[i][0]))
    print('hello world')
    # except psycopg2.Error as e:
    #     print(e)
    # finally:
    #     conn.close()
    #dist_list = (('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
    return lookuplist



# functiosn for metadata tab
def uc_handle(request):
    def insertIntoLookup(divname, distName, talukName, ucName, low_priority):
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']

        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)

        cursor = conn.cursor()
        sql="select insertLookup('" + str(divname) + "','" + str(distName) + "','" + str(talukName) + "','" + str(
            ucName) + "', '"+low_priority+"'   )"
        print (sql)
        cursor.execute(sql)
        conn.commit()
        cursor.close()




    form = AddUcsMeta()
    if request.method=='POST':
        form = AddUcsMeta(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            new_division_name= cd.get('division_name')
            new_district_name = cd.get('District_Name')
            new_taluka_name = cd.get('Taluka_Name')
            new_uc_name=cd.get('Union_Councile_Name')
            priority=''
            if cd.get('Low_Priority'):
                priority='Yes'
            else:
                priority='No'

            insertIntoLookup(new_division_name,new_district_name,new_taluka_name,new_uc_name, priority)




            return HttpResponseRedirect(reverse("uc_handle"))
    else:
        form = AddUcsMeta()
    return render(request, "feedback/addUCS.html", {'form': form})


# Editing the existing records
@login_required
def updatesurveyors(request):
    if request.POST:
        queryset = Surveyors.objects.filter(pk=request.POST["val"]).first()
        form = SurveyorMetaForm(request.POST, instance=queryset)
        if form.is_valid():
            form.save()
            return redirect(reverse('showSurveyors'))
    else:
        queryset = Surveyors.objects.filter(pk=request.GET["id"]).first()
        form = SurveyorMetaForm(instance=queryset)

    template = 'feedback/updateSurveyors.html'
    kwvars = {
        'form': form,
        'pkval': request.GET["id"]
    }
    return render(request, template, kwvars)


# deleting the records
@login_required
def project_delete(request, pk, template_name='webprogress/project_confirm_delete.html'):
    server = get_object_or_404(Surveyors, pk=pk)
    if request.method == 'GET':
        server.delete()
        return redirect('showSurveyors')
    return HttpResponseRedirect(reverse('showSurveyors'))


@login_required
def updateteamleads(request):
    if request.POST:
        queryset = TeamLeads.objects.filter(pk=request.POST["val"]).first()
        form = TeamLeadsMetaForm(request.POST, instance=queryset)
        if form.is_valid():
            form.save()
            return redirect(reverse('showTeamLeads'))
    else:
        queryset = TeamLeads.objects.filter(pk=request.GET["id"]).first()
        form = TeamLeadsMetaForm(instance=queryset)

    template = 'feedback/updateTeamLeads.html'
    kwvars = {
        'form': form,
        'pkval': request.GET["id"]
    }
    return render(request, template, kwvars)


# deleting the records
@login_required
def teamleads_delete(request, pk, template_name='webprogress/project_confirm_delete.html'):
    server = get_object_or_404(TeamLeads, pk=pk)
    if request.method == 'GET':
        server.delete()
        return redirect('showTeamLeads')
    return HttpResponseRedirect(reverse('showTeamLeads'))
