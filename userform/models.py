from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from feedback import settings
from django.contrib.postgres import fields
from django.db.models import Q
import datetime
import psycopg2


_CHOICE_GENDER = (('F','Female'),('M','Male'))
_CHOICE_TPL = (('Y','Yes'),('N','No'))
RATING_CHOICES = ((0, 'Never used'),(1, 'Bad'),(2, 'Below Average'),(3, 'Average'),(4, 'Above Average'),(5, 'Good'))
_DIST_LIST = (('Mirpur', 'Mirpur'),('Khairpur', 'Khairpur'),('Shikarpur', 'Shikarpur'),('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
_ACTIVITY_LIST = (('Activity_1','Activity_1'),('Activity_2','Activity_2'),('Activity_3','Activity_3'),('Activity_4','Activity_4'))
_TALUKA_LIST = (('Taluka-1', 'Taluka-1'),('Taluka-2', 'Taluka-2'),('Taluka-3', 'Taluka-3'),('Taluka-4', 'Taluka-4'),('Taluka-5', 'Taluka-5'))
_UC_LIST = (('UC-1', 'UC-1'),('UC-2', 'UC-2'),('UC-3', 'UC-3'),('UC-4', 'UC-4'),('UC-5', 'UC-5'))


class FeedbackQuerySet(models.QuerySet):
    def all_feedback(self):
        return self.all()

class FeedbackMeta(models.Model):
    def getLookupValue(self,type):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        try:
            conn = psycopg2.connect(database=dbname,
                                    user=username,host=host,
                                    password=password,port=port)
            #conn.set_isolation_level(ISOLEVEL)
            cursor = conn.cursor()
            if(type=='dist'):
                cursor.execute("select district,district from lookup group by district order by district")
            elif(type=='tehs'):
                cursor.execute("select tehsil,tehsil from lookup group by tehsil order by tehsil")
            elif(type=='uc'):
                cursor.execute("select uc,uc from lookup group by uc order by uc")
            fetch = cursor.fetchall()

            #dblist = [fetch[i][0] for i in range(len(fetch))]
            lookuplist = []
            for i in range(len(fetch)):
                lookuplist.append((fetch[i][1], fetch[i][0]))
            print('hello world')
        except psycopg2.Error as e:
            print(e)
        finally:
            conn.close()
        #dist_list = (('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
        return lookuplist

    def getSurveyorsLookup(self):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)
        #conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        cursor.execute('select "Sur_Name","Sur_Name" from userform_surveyors order by "Sur_Name"')
        fetch = cursor.fetchall()

        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append((fetch[i][1], fetch[i][0]))
        conn.close()
        return lookuplist


    activity = models.CharField(max_length=100,default="",verbose_name="Specify Activity Name")
    dist = models.CharField(max_length=100,default="Karachi", choices=getLookupValue('lookup','dist'), verbose_name="District")
    taluka = models.CharField(max_length=100,default="Taluka-1", choices=getLookupValue('lookup','tehs'), verbose_name="Taluka")
    uc = models.CharField(max_length=255,default="UC-1", verbose_name="Union Council")
    Surveyors= models.CharField(max_length=50,  default="")
    TeamLeads = models.CharField(max_length=50, default="")
    visit_date = models.DateField(verbose_name="Visit Date")


class Feedback(models.Model):

    def get_Activities(self):
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']

        conn = psycopg2.connect(database=dbname,
                                user=username,host=host,
                                password=password,port=port)
        #conn.set_isolation_level(ISOLEVEL)
        cursor = conn.cursor()
        #cursor.execute("select * from lookuo")
        cursor.execute("select activity,activity from userform_feedbackmeta order by activity")
        fetch= cursor.fetchall()
        lookuplist = []
        for i in range(len(fetch)):
            lookuplist.append((fetch[i][1], fetch[i][0]))
        return lookuplist



    def getLookupValue(self,type):
        #print(settings.DATABASES)
        dbname = settings.DATABASES['default']['NAME']
        username = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        port = settings.DATABASES['default']['PORT']
        try:
            conn = psycopg2.connect(database=dbname,
                                    user=username,host=host,
                                    password=password,port=port)
            #conn.set_isolation_level(ISOLEVEL)
            cursor = conn.cursor()
            if(type=='dist'):
                cursor.execute("select district,district from lookup  order by district")
            elif(type=='tehs'):
                cursor.execute("select tehsil,tehsil from lookup  order by tehsil")
            elif(type=='uc'):
                cursor.execute("select uc,uc from lookup  order by uc")
            fetch = cursor.fetchall()

            #dblist = [fetch[i][0] for i in range(len(fetch))]
            lookuplist = []
            for i in range(len(fetch)):
                lookuplist.append((fetch[i][1], fetch[i][0]))
            print('hello world')
        except psycopg2.Error as e:
            print(e)
        finally:
            conn.close()
        #dist_list = (('Hyderabad', 'Hyderabad'),('Karachi', 'Karachi'))
        return lookuplist

    from_user = models.CharField(max_length=30, null=False)


    activity = models.CharField(max_length=30,default="",verbose_name="Choose Activity")
    name = models.CharField(max_length=100, default='', verbose_name="Surveyor's Name")
    contact = models.CharField(max_length=30, default='0', verbose_name="Contact Number")

    dist = models.CharField(max_length=30,default="Karachi", choices=getLookupValue('lookup','dist'), verbose_name="District")
    taluka = models.CharField(max_length=30,default="Taluka-1", choices=getLookupValue('lookup','tehs'), verbose_name="Taluka")
    uc = models.CharField(max_length=30,default="UC-1", choices=getLookupValue('lookup','uc'), verbose_name="Union Council")
    Surveyor= models.CharField(max_length=30,default="",  verbose_name="Survyor")
    TeamLeads = models.CharField(max_length=30, default="",
                                verbose_name="Team Lead Name")
    visit_date = models.DateField(blank=True, verbose_name="Start Visit Date")
    remarks = models.CharField(max_length=30,default="", null=True, verbose_name="Remarks")
    objects = FeedbackQuerySet.as_manager()


class Surveyors(models.Model):
    id=models.AutoField(primary_key=True)
    Sur_Name = models.CharField(max_length=30,default="",verbose_name="Surveyor's Name")
    Sur_Age = models.IntegerField(blank=False, verbose_name="Surveyor's Age")
    Sur_CNIC = models.CharField(max_length=30, default="", verbose_name="Surveyor's CNIC")
    Sur_CONTACT = models.CharField(max_length=30, default="", verbose_name="Surveyor's Contact")
    Sur_Date= models.DateField(blank=True, verbose_name="Date")

    def __str__(self): return self.Sur_Name

class TeamLeads(models.Model):
    id = models.AutoField(primary_key=True)
    TL_Name = models.CharField(max_length=50, default="", verbose_name="Team Lead's Name")
    TL_Age = models.IntegerField(blank=False, verbose_name="Team Lead's Age")
    TL_CNIC = models.CharField(max_length=50, default="", verbose_name="Team Lead's CNIC")
    TL_CONTACT = models.CharField(max_length=30, default="", verbose_name="Team Lead's Contact")
    TL_Date = models.DateField(blank=True, verbose_name="Date")

    def __str__(self): return self.TL_Name