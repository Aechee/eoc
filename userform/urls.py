from django.conf.urls import url
from django.contrib.auth.views import LoginView,LogoutView

from .views import *

urlpatterns = [
url(r'^home', index, name='index'),
    url(r'^feedback$', new_feedback, name='new_feedback'),
    url(r'^metafeedback$', meta_feedback, name='meta_feedback'),
    url(r'^firstreport$', showFirstReport, name='firstreport'),
    url(r'^showSurveyors$', showSurveyors, name='showSurveyors'),
    url(r'^showTeamLeads$', showTeamLeads, name='showTeamLeads'),
    url(r'^showUC$', showUCs, name='showUCS'),
    url(r'^secondreport$', showSecondReport, name='secondreport'),
    url(r'login$', LoginView.as_view(template_name='feedback/login_form.html'), name='player_login'),
    url(r'^$', LoginView.as_view(template_name='feedback/login_form.html'), name='player_login'),
    url(r'logout$',LogoutView.as_view(),name='feedback_logout'),
   # url(r'signup$', SignupView.as_view(),name='player_signup'),
    url(r'showFeedback$',home, name="player_home"),
    url(r'^update_taluka$', update_talukaFn, name='update_taluka'),
    url(r'^update_union$', update_ucFn, name='update_uc'),
    url(r'^update_activities$', update_activitites, name='update_activities'),
    url(r'^update_district$', update_districts, name='update_district'),
    url(r'^update_taluka_feedback$', update_taluka_feedback, name='update_taluka_feedback'),
    url(r'^update_uc_feedback', update_uc_feedback, name='update_uc_feedback'),
    url(r'^addUCMeta', uc_handle, name='uc_handle'),
    url(r'^addSurveyorMeta', surveyor_handle, name='surveyor_handle'),
    url(r'^addTeamleadsMeta', teamleads_handle, name='teamLeads_handle'),
    url(r'^update_surveyors_feedback', update_surveyors_feedback, name='update_surveyors_feedback'),
    url(r'^update_teamleads_feedback', update_teamleads_feedback, name='update_teamleads_feedback'),
url(r'^updatesurveyors', updatesurveyors, name='updatesurveyors'),
url(r'^surveyor/delete/(?P<pk>\d+)$', project_delete, name='surveyor_delete'),
url(r'^updateteamleads', updateteamleads, name='updateteamleads'),
url(r'^teamleads/delete/(?P<pk>\d+)$', teamleads_delete, name='teamlead_delete'),



]

